import socket
import struct
import binascii
import time
import signal
import sys
import math

suma = 0
tiempo = 0
primera = 1

# Creamos el socket
with socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x0800)) as s:
#    s.bind(('eth1', socket.htons(0x0800)))
#    sock_size = s.getsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF)
#    print(f'Tamaño del socket {sock_size}')


    s.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, str("eth1" + '\0').encode('utf-8'))


    while 1:
        # Recibimos datos
        pkt = s.recv(1500)
        suma += len(pkt)

        if primera:
           # print("Por aquí sólo pasa una vez")
            primera = 0
            inicial = time.time()

        final = time.time()
        tiempo = final - inicial

        throughput = ((suma * 8) / tiempo) / 1000000

        if (math.floor(tiempo) % 30) == 0:
            #print(f"Bytes {len(pkt)}")
            #print(f"Tiempo {tiempo}")
            print(f"El throughput es de {throughput} Mbps")
