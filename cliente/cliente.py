#!/usr/bin/python

import socket
import struct
import time
import os
import multiprocessing

def info(title):
    print(title)
    print('Nombre del móulo:', __name__)
    print('Proceso padre:', os.getppid())
    print('ID del proceso:', os.getpid())

def crea_paquete(mtu):
    payload = bytearray(mtu - 14)
    # Creamos el packete Ethernet
    header = struct.pack("!6s6s2s", b'\xaa\xaa\xaa\xaa\xaa\xaa', b'\xbb\xbb\xbb\xbb\xbb\xbb', b'\x08\x00')
    paquete = header + payload
    print(f"Tamaño del paquete enviado: {len(paquete)}")

    return paquete

def envia_paquete(paquete):

    # Creamos el RAW socket
    # PF_SOCKET (packet interface), SOCK_RAW (Raw socket) - htons (protocol) 0x08000 = IP Protocol
    with socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x0800)) as s:

        # Asociamos el socket a la interfaz
        s.bind(('eth1', socket.htons(0x0800)))

        while 1:
            # Enviamos el paquete a través de la interfaz
            try:
                s.send(paquete)
            except OSError as error:
                print(error) 


def genera_trafico():
    info('Función genera_trafico')
    p = crea_paquete(1500)
    envia_paquete(p)

if __name__ == '__main__':
    
    info('Código principal')
    p = multiprocessing.Process(target=genera_trafico, args=())
    p.start()
